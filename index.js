const express = require('express'),
    app = express(),
    template = require('./build/template')
    path = require('path'),
    ssr = require('./build/server'),

// Serving static files
app.use('/assets/styles', express.static(path.resolve(__dirname, 'assets/styles')));
app.use('/assets/js', express.static(path.resolve(__dirname, 'assets/js')));
app.use('/assets/images', express.static(path.resolve(__dirname, 'assets/images')));


// hide powered by express
app.disable('x-powered-by');
// start the server
app.listen(process.env.PORT || 3000);

let initialState = {
    isFetching: false,
    apps: {}
}

// server rendered home page
const home = require("./build/page/home")
app.get('/', (req, res) => {
    const { preloadedState, content}  = ssr()
    const response = template("Home", content, preloadedState)
    res.setHeader('Cache-Control', 'assets, max-age=604800')
    res.send(response);
});

// app.get('/react', (req, res) => {
//     const { preloadedState, content}  = ssr(initialState)
//     const response = template("Server Rendered Page", preloadedState, content)
//     res.setHeader('Cache-Control', 'assets, max-age=604800')
//     res.send(response);
// });