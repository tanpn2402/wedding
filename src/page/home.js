import React from "react"
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption,
    Button,
    Col,
    Row
  } from 'reactstrap';
import $ from "jquery"
import FlipClock from "../components/FlipClock"




const items = [
    {
        src: 'http://clarissa.javatheme.com/img/blog-img-2.jpg',
        altText: 'Slide 1',
        caption: 'Slide 1'
    },
    {
        src: 'http://clarissa.javatheme.com/img/event-img-1.jpg',
        altText: 'Slide 2',
        caption: 'Slide 2'
    },
    {
        src: 'http://clarissa.javatheme.com/img/banner-img.jpg',
        altText: 'Slide 3',
        caption: 'Slide 3'
    }
]

class HeaderCarousel extends React.Component {

    constructor(props) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }

    onExiting() {
        this.animating = true;
    }
    
    onExited() {
        this.animating = false;
    }
    
    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }
    
    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }
    
    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }

    render() {
        const { activeIndex } = this.state;
        const slides = items.map((item) => {
            return (
                <CarouselItem
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src} >
                    <img src={item.src} alt={item.altText} />
                </CarouselItem>
            );
        });

        return (
                <Carousel
                    activeIndex={activeIndex}
                    next={this.next}
                    previous={this.previous}>
                    <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                    {slides}
                </Carousel>
        )
    }
}

class MenuNav extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <React.Fragment>
                <div className="brand">
                    <ul className="name">
                        <li><span>Nhật Tân</span></li>
                        <li className="and">&</li>
                        <li><span>Phượng Hằng</span></li>
                    </ul>

                    {/* <ul className="name sm">
                        <li><span>T</span></li>
                        <li className="and">&</li>
                        <li><span>H</span></li>
                    </ul> */}
                </div>
                <div className="hamburger-icon" ref={n => this.hamburger = n} onClick={e => this.toggleNavList()}>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>

                <div className="nav__link-list" ref={n => this.navList = n}>
                    <a className="navigation__link active" href="#welcome" onClick={e => this.props.onClick(e)}>Welcome</a>
                    <a className="navigation__link" href="#about" onClick={e => this.props.onClick(e)}>Giới thiệu</a>
                    <a className="navigation__link" href="#story" onClick={e => this.props.onClick(e)}>Câu chuyện</a>
                    <a className="navigation__link" href="#event" onClick={e => this.props.onClick(e)}>Sự kiện</a>
                    <a className="navigation__link" href="#gallery" onClick={e => this.props.onClick(e)}>Thư viện</a>
                    <a className="navigation__link" href="#map" onClick={e => this.props.onClick(e)}>Bản đồ</a>
                    <a className="navigation__link" href="#thanks" onClick={e => this.props.onClick(e)}>Lời cám ơn</a>
                </div>
                
            </React.Fragment>
        )
    }

    toggleNavList() {
        this.hamburger.classList.toggle("open")
        this.navList.classList.toggle("open")
        
    }
}

class About extends React.Component {
    render() {
        return (
            <Row>
                <Col lg={2} md={1} sm={12} xs={12}></Col>
                <Col lg={4} md={5} sm={12} xs={12} className="box">
                    <div className="header">
                        <h2>Phạm Nhật Tân</h2>
                    </div>
                    <div className="avatar">
                        <div className="img grooms"></div>
                    </div>
                    <div className="description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris porttitor non sem eu faucibus. Aenean pellentesque dui tincidunt, dictum massa congue, euismod ipsum. Duis auctor in est ac dignissim.
                    </div>
                </Col>
                <Col lg={4} md={5} sm={12} xs={12} className="box">
                    <div className="header">
                        <h2>Trần Thị Phượng Hằng</h2>
                    </div>
                    <div className="avatar">
                        <div className="img brides"></div>
                    </div>
                    <div className="description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris porttitor non sem eu faucibus. Aenean pellentesque dui tincidunt, dictum massa congue, euismod ipsum. Duis auctor in est ac dignissim.
                    </div>

                </Col>
                <Col lg={2} md={1} sm={12} xs={12}></Col>
            </Row>
        )
    }
}


class StoryItem extends React.Component {

    render() {
        return (
        <React.Fragment>
            <Row className="story-item">
                
                <Col md={6} sm={12} className="left event">
                    <div className="heart"><img src={`/assets/images/heart.png`} /></div>
                    <div className="des">
                    <h2>Lần gặp đầu tiên</h2>
                    <h3>14-02-2013</h3>
                        "Trong một buổi đi chơi cùng nhóm bạn thời cấp một, tôi vô tình gặp được cô ấy, ấn tượng đầu của tôi là cô ấy giống như một cô bé, nhỏ nhắn, xinh xắn và dễ thương"
                        <div>Nhật Tân</div>
                    </div>
                </Col>
                <Col md={6} sm={12} className="right image">
                    <img src={'http://irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/images/couple/img-1.jpg'} />
                </Col>
            </Row>

            <Row className="story-item">
                <Col md={6} sm={12} className="left image">
                <img src={'http://irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/images/couple/img-1.jpg'} />
                </Col>
                <Col md={6} sm={12} className="right event">
                    
                    <div className="heart"><img src={`/assets/images/heart.png`} /></div>
                    <div className="des">
                    <h2>Buổi đi chơi đầu tiên</h2>
                    <h3>14-02-2013</h3>
                        Sau khoảng thời gian nhắn tin, tìm hiểu, chúng tôi cùng lên kế hoạch đi chơi chung với nhau, từ đây, chúng tôi nhận ra tình cảm thực sự dành cho nhau.
                    </div>
                </Col>
            </Row>

            <Row className="story-item">
                <Col md={6} sm={12} className="left event">
                    <div className="heart"><img src={`/assets/images/heart.png`} /></div>
                    <div className="des">
                    <h2>Yêu xa</h2>
                    <h3>14-09-2014</h3>
                        Đây là khoảng thời gian chúng tôi xa cách nhau, bắt đầu cho những sự khó khăn, ngăn cách, tuy nhiên, với sự tin tưởng, yêu thương dành cho nhau, chúng tôi đã vượt qua tất cả.    
                    </div>
                </Col>
                <Col md={6} sm={12} className="right image">
                    <img src={'http://irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/images/couple/img-1.jpg'} />
                </Col>
            </Row>

            <Row className="story-item">
                <Col md={6} sm={12} className="left image">
                    <img src={'http://irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/images/couple/img-1.jpg'} />
                </Col>
                <Col md={6} sm={12} className="right event">
                    <div className="heart"><img src={`/assets/images/heart.png`} /></div>
                    <div className="des">
                    <h2>Chia cách</h2>
                    <h3>14-09-2014</h3>
                        Tuy vẫn còn tình cảm cho nhau, nhưng cái tôi của chúng tôi quá lớn, chúng tôi đã không thông cảm cho nhau, vì vậy chúng tôi đã quyết định chia cách nhau trong một thời gian để suy nghĩ lại tình cảm của mình.
                    </div>
                </Col>
            </Row>

            <Row className="story-item">
                <Col md={6} sm={12} className="left event">
                    <div className="heart"><img src={`/assets/images/heart.png`} /></div>
                    <div className="des">
                    <h2>Yêu lại từ đầu</h2>
                    <h3>14-09-2014</h3>
                        Sau khoảng thời gian hơn 1 năm chia cách, chúng tôi nhận ra mình không thể yêu thêm một ai nữa, do đó chúng tôi quyết định tìm lại nhau, cùng nhau xây lại yêu thương, và kết quả là gì thì các bạn cũng biết rồi đó.   
                    </div>
                </Col>
                <Col md={6} sm={12} className="right image">
                    <img src={'http://irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/images/couple/img-1.jpg'} />
                </Col>
            </Row>
        </React.Fragment>
        )
    }
}

class Story extends React.Component {
    render() {
        return (
            <Row>
                <Col lg={1} md={1} sm={1} xs={12}></Col>
                <Col lg={10} md={10} sm={10} xs={12} className="box">
                    <StoryItem />
                </Col>
                <Col lg={1} md={1} sm={1} xs={12}></Col>
            </Row>
        )
    }
}

class Qoute extends React.Component {

    render() {
        return (
            <div className="page-section qoute" >
                { this.props.children }
            </div>
        )
    }
}

class Event extends React.Component {
    render() {
        return (
            <Row>
                <Col lg={1} md={1} sm={1} xs={12}></Col>
                <Col lg={10} md={10} sm={10} xs={12}>
                    <Row className="event-row">
                        <Col md={4} sm={12}>
                            <img src="/assets/images/church.jpg" />
                        </Col>

                        <Col md={8} sm={12}>
                            <h2>Lễ cưới</h2>
                            <div className="flex-row"> 
                                <img src="/assets/images/placeholder.png" style={{}}/>
                                Nhà thờ GX Thiên Long, Bàu Cạn, Long Thành, Đồng Nai
                            </div>
                            <div className="flex-row">
                                <img src="/assets/images/time.png" style={{ width: "16px", height: "16px" }} />
                                09:00 sáng, 03-07-2018
                            </div>
                        </Col>
                    </Row>
                    <div className="sep"><div></div></div>
                    <Row className="event-row">
                        <Col md={4} sm={12}>
                            <img src="/assets/images/church.jpg" />
                        </Col>

                        <Col md={8} sm={12}>
                            <h2>Tiệc cưới - Nhà gái</h2>
                            <div className="flex-row"> 
                                <img src="/assets/images/placeholder.png" style={{}}/>
                                Tư gia Ấp 2, Bàu Cạn, Long Thành, Đồng Nai
                            </div>
                            <div className="flex-row">
                                <img src="/assets/images/time.png" style={{ width: "16px", height: "16px" }} />
                                11:00 sáng, 04-07-2018
                            </div>
                        </Col>
                    </Row>
                    <div className="sep"><div></div></div>
                    <Row className="event-row">
                        <Col md={4} sm={12}>
                            <img src="/assets/images/church.jpg" />
                        </Col>

                        <Col md={8} sm={12}>
                            <h2>Tiệc cưới - Nhà trai</h2>
                            <div className="flex-row"> 
                                <img src="/assets/images/placeholder.png" style={{}}/>
                                Tư gia Ấp 8, Bàu Cạn, Long Thành, Đồng Nai
                            </div>
                            <div className="flex-row">
                                <img src="/assets/images/time.png" style={{ width: "16px", height: "16px" }} />
                                11:00 sáng, 05-07-2018
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col lg={1} md={1} sm={1} xs={12}></Col>
            </Row>
        )
    }
}

export default class Home extends React.Component {
    constructor(props) {
        super(props)


        this.lastScroll = 0;

    }
    

    render() {
        

        return (
            <div id="homePage" key="root" onScroll={() => this.onScroll()} ref={n => this.node = n}>
                <div id="imageCarousel" ref={n => this.imageCarosel = n}>
                    <div className="title-over">
                        <div className="wrapper">
                            <div className="overlay"></div>
                            {/* <div className="save-date"> */}
                                <h2 className="save-date">save the date</h2>
                            {/* </div> */}
                            <ul className="name">
                                <li><span>Nhật Tân</span></li>
                                <li className="and">&</li>
                                <li><span>Phượng Hằng</span></li>
                            </ul>
                            <div className="date">Th.8 <span>04 - 05</span> 2018</div>
                            <FlipClock />
                        </div>
                    </div>
                    <HeaderCarousel />
                </div>
                <div className="mainPage">
                    <div id="menuNav" ref={n => this.menuNav = n} style={{color: "red"}}>
                        <MenuNav onClick={e => this.onMenuClick(e)}/>
                    </div>

                    <div id="content">
                        <Qoute>Counting the minutes for the big day!</Qoute>
                        <div className="page-section" id="about">

                            <div className="title">Perfect <span>Couple</span></div>
                            <About />
                        </div>
                        <div className="page-section" id="story">
                            <div className="title">Our <span>Story</span></div>
                            <Story />
                        </div>
                        <Qoute>Counting the minutes for the big day!</Qoute>
                        <div className="page-section" id="event">
                            <div className="title">Wedding <span>Events</span></div>
                            <Event />
                        </div>
                        <div className="page-section" id="gallery">

                        </div>
                        {/* <Qoute>Counting the minutes for the big day!</Qoute> */}
                        <div className="page-section" id="map">

                        </div>
                        <div className="page-section" id="thanks">

                        </div>
                        <Qoute>Thanks you!</Qoute>
                    </div>
                </div>
            </div>
        )
    }

    onMenuClick(e) {
        e.preventDefault();
        let target = $(e.target).attr("href")
        console.log(target)
        $('#homePage').animate({
            scrollTop: $(target).offset().top
        }, 600, function() {
                // location.hash = target;
        });

    }

    onScroll() {

        
        if(this.node.scrollTop > this.imageCarosel.offsetHeight) {
            this.menuNav.classList.add("goToTop")
        } else {
            this.menuNav.classList.remove("goToTop")
        }
    }
    
}


