
// html skeleton provid er
function template(title, content = "", state = {}){
    let scripts = ` <script>
                        window.__STATE__ = ${JSON.stringify(state)}
                    </script>
                    <script src="assets/js/main.js"></script>
                `
    let page = `<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title> ${title} </title>
                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
                    <link href="assets/styles/style.css" rel="stylesheet">
                    <meta name="google-site-verification" content="qE1qNLn82RtfjKrtO-y8kh1G-mn2uqB65eGCY-EMSbM" />
                </head>
                <body>
                    
                    <div id="root" class="wrap-inner">
                        ${content}
                    </div>
                    </div>
                    ${scripts}
                </body>
              `;

  return page;
}

module.exports = template;
